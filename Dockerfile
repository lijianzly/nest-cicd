FROM node:20.5.0-alpine3.18

# 设置工作目录
WORKDIR /app

# 拷贝package.json 和 lock 文件到工作目录中
COPY package.json .
COPY pnpm-lock.yaml .

RUN npm config set registry https://registry.npmmirror.com 

RUN npm install -g pnpm

RUN npm install -g pm2

# 安装生产依赖
RUN pnpm install --production

# 将 CI 构建任务的制品 dist 目录，拷贝到工作目录中的 dist 目录下  
COPY ./dist ./dist

# 暴露3000端口
# 暴露的端口号需要和 Nest 应用监听的端口号保持一致
EXPOSE 3000

# 启动容器后执行的命令
CMD ["pm2-runtime", "dist/main.js"]
